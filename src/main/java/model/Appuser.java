package model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name="APPUSER")
public class Appuser {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Size(min=3, max=50)
	@Column(name = "SURNAME", nullable = false)
	private String surname;

	@Size(min=3, max=50)
	@Column(name = "FIRSTNAME", nullable = false)
	private String firstname;

	@NotNull
	@DateTimeFormat(pattern="dd.MM.yyyy") 
	@Column(name = "BIRTHDATE", nullable = false)
	private Date birthdate;

	@Size(min=4, max=6)
	@Column(name = "GENDER", nullable = false)
	private String gender;
	
	@Size(min=3, max=50)
	@Column(name = "EMAIL", nullable = false)
	private String email;
	
	@Size(min=3, max=50)
	@Column(name = "USERNAME", nullable = false)
	private String username;
	
	@Size(min=3, max=50)
	@Column(name = "PASSWORD", nullable = false)
	private String password;

	public String getEmail() {
		return email;
	}



	public void setEmail(String email) {
		this.email = email;
	}



	public String getUsername() {
		return username;
	}



	public void setUsername(String username) {
		this.username = username;
	}



	public String getPassword() {
		return password;
	}



	public void setPassword(String password) {
		this.password = password;
	}



	public String getGender() {
		return gender;
	}



	public void setGender(String gender) {
		this.gender = gender;
	}



	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public String getSurname() {
		return surname;
	}



	public void setSurname(String surname) {
		this.surname = surname;
	}



	public String getFirstname() {
		return firstname;
	}



	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}



	public Date getBirthdate() {
		return birthdate;
	}



	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}
	
	

	//1. TODO Properties
	//2. TODO getters and setters
	
	
}
